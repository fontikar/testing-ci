#!/bin/bash

# Patterns to match here seperated by | OR operator
pattern="does not appear to have|error|not found"

FILE_PATH=$(find . -type f -name '*.log' -print -quit)
echo "File path (before grep): $FILE_PATH"

# If statement checking if patterns exist in log file
if grep -q -E "${pattern}" "$FILE_PATH" ; then
    echo "There are experiments with missing elements"
    grep -i -E "${pattern}" "$FILE_PATH" | sort |uniq -c  > grep_results.txt
    ls -l ./
    echo "Check complete"
    exit 64
else
    echo "No issues with the log file - test passed"
    echo "Check complete"
    exit 0
fi